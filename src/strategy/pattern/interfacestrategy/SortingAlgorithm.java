/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.pattern.interfacestrategy;

import java.util.ArrayList;

/**
 *
 * @author Ruth
 */
public interface SortingAlgorithm {
    public ArrayList<Integer> SortingAlgorithm(ArrayList<Integer> list);
}
