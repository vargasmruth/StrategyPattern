/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.pattern.context;

import java.util.ArrayList;
import java.util.Random;
import strategy.pattern.concretestrategy.BubbleSort;
import strategy.pattern.concretestrategy.InsertionSort;
import strategy.pattern.concretestrategy.QuickSort;
import strategy.pattern.interfacestrategy.SortingAlgorithm;

/**
 *
 * @author Ruth
 */
public class SortArray {
    
    private SortingAlgorithm sortingAlgorithm;

    public SortArray( ) {

    }

    public ArrayList<Integer> GenerateRandomList(int listSize, int rangeNumbers){
        ArrayList<Integer> numbers = new ArrayList();
        for (int i = 0; i < listSize; i++) {
            Random numberGenerate = new Random();
            int Rnumber = 1+numberGenerate.nextInt(rangeNumbers);
            numbers.add(Rnumber);            
        }
        return numbers;
    }
    
    public void Sort(ArrayList<Integer> array, String name) {
        switch(name)
     {
        case "BubbleSort":
	  sortingAlgorithm = new BubbleSort();
         sortingAlgorithm.SortingAlgorithm(array);
        break;
	case "QuickSort":
	  sortingAlgorithm = new QuickSort();
          sortingAlgorithm.SortingAlgorithm(array);
            break;
	case "InsertionSort":
	  sortingAlgorithm = new InsertionSort();
          sortingAlgorithm.SortingAlgorithm(array);
            break;
          
      }
    }
 
    
    
    
}
