/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.pattern.main;

import java.util.ArrayList;
import strategy.pattern.context.SortArray;
import strategy.pattern.interfacestrategy.SortingAlgorithm;


/**
 *
 * @author Ruth
 */
public class StrategyPattern {
    
    public static void main(String[] args) {
        SortArray array= new SortArray();
        ArrayList<Integer> lista = array.GenerateRandomList(10, 20);

        System.out.println("LISTA INICIO:"+ lista);
        
        array.Sort(lista, "BubbleSort");
        System.out.println("BubbleSort --->"+ lista);

        array.Sort(lista, "QuickSort");
        System.out.println("QuickSort --->"+ lista);

        array.Sort(lista, "InsertionSort");
        System.out.println("InsertionSort --->"+ lista);

    }
    
}
