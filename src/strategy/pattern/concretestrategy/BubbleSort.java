/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.pattern.concretestrategy;

import java.util.ArrayList;
import strategy.pattern.interfacestrategy.SortingAlgorithm;

/**
 *
 * @author Ruth
 */
public class BubbleSort implements SortingAlgorithm{

     @Override
    public ArrayList<Integer> SortingAlgorithm(ArrayList<Integer> list) {
        if (list.size()>1)
        {
            for (int i=0; i<list.size(); i++) // bubble sort outer loop
            {
                for (int j=1; j < list.size()-i; j++) {
                    if (list.get(j)<(list.get(j-1)))                    {
                        Integer aux1;Integer aux2;Integer aux3;
                        aux1 = list.get(j);
                        aux2 = list.get(j-1);
                        aux3=aux1;
                        aux1=aux2;
                        aux2=aux3;
                        list.set(j,aux1);
                        list.set(j-1,aux2);
                    }
                }
            }
        }
    return list;
    }


}
