package strategy.pattern.concretestrategy;

import strategy.pattern.interfacestrategy.SortingAlgorithm;

import java.util.ArrayList;

/**
 * Created by Ruth on 16/4/2018.
 */
public class InsertionSort implements SortingAlgorithm {

    @Override
    public ArrayList<Integer> SortingAlgorithm(ArrayList<Integer> list) {
        int aux; int c1; int c2;
        for (int i = 1; i < list.size(); i++) {
            aux = list.get(i);
            for (int j = i-1; j >= 0 && list.get(j) > aux; j--) {
                list.set(j+1,list.get(j));
                list.set(j,aux);
            }
        }
        return list;
    }
}
