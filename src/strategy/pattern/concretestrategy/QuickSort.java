/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy.pattern.concretestrategy;

import java.util.ArrayList;
import strategy.pattern.interfacestrategy.SortingAlgorithm;

/**
 *
 * @author Ruth
 */
public class QuickSort implements SortingAlgorithm{
    @Override
    public ArrayList<Integer> SortingAlgorithm(ArrayList<Integer> list) {
        return quickSort1(list);
    }

    private ArrayList<Integer> quickSort1(ArrayList<Integer> list) {
        return quickSort2(list, 0 , list.size()-1);
    }

    private ArrayList<Integer> quickSort2(ArrayList<Integer> list, int inicio, int fin) {
        if(inicio >= fin) return list;
        int i=inicio; int f=fin;
        if (inicio!=fin){
            int pivote; int aux;
            pivote = inicio;
            while (inicio!=fin){
                while (list.get(fin) >= list.get(pivote) && inicio < fin)
                    fin--;
                    while (list.get(inicio) < list.get(pivote) && inicio<fin)
                        inicio++;

                if (fin!=inicio){
                    aux = list.get(fin);
                    list.set(fin,list.get(inicio));
                    list.set(inicio,aux);
                }
                if (inicio == fin){
                    quickSort2(list, i,inicio-1);
                    quickSort2(list, inicio+1, f);
                }
            }
        }else{
            return list;
        }

        return list;

        }



}
